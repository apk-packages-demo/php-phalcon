# php-phalcon

Web framework delivered as a C extension. https://pkgs.alpinelinux.org/packages?name=php*-phalcon&arch=x86_64

[![PHPPackages Rank](http://phppackages.org/p/phalcon/cphalcon/badge/rank.svg)](http://phppackages.org/p/phalcon/cphalcon)
[![PHPPackages Referenced By](http://phppackages.org/p/phalcon/cphalcon/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/cphalcon)
* https://packagist.org/packages/phalcon/cphalcon

## Official documentation
* [*Installation*](https://docs.phalcon.io/latest/en/installation)
  * Also on Debian Sury https://gitlab.com/apt-packages-demo/php-phalcon
* [*Installation*](https://phalcon-php-framework-documentation.readthedocs.io/en/latest/reference/install.html)
  (readthedocs.io)

## Unofficial documentation
* [*Phalcon (framework)*](https://en.m.wikipedia.org/wiki/Phalcon_(framework))
  (Wikipedia)

### Tutorial
* [phalcon tutorial](https://www.google.com/search?q=phalcon+tutorial)
* [*Phalcon Tutorial*](https://www.tutorialspoint.com/phalcon/index.htm)